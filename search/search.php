<?php
if (!defined('WEB_ROOT')) {
	exit;
}

if (isset($_GET['book']) && ($_GET['chapter']) && (int)$_GET['chapter'] >= 0 && ($_GET['verse']) && (int)$_GET['verse'] >= 0) {
	$book = $_GET['book'];
	$chapter = (int)$_GET['chapter'];
	$verse = (int)$_GET['verse'];
	$queryString = "book=$book&chapter=$chapter&verse=$verse";
} else if (isset($_GET['passage']) && ($_GET['chapter']) && (int)$_GET['chapter'] >= 0){
	$book = $_GET['passage'];
	$chapter = (int)$_GET['chapter'];
	$verse = 0;
	$queryString = '';
	$queryString = "passage=$book&chapter=$chapter"; 
}else {
	$book = '';
	$chapter = 0;
	$verse = 0;
	$queryString = "book=Genesis&chapter=1&verse=1"; 
	//header('Location: index.php?error=' . urlencode('No search done yet')); 
}


//log request
dbQuery("INSERT INTO searchlogs(clientaddress,clientdevice) VALUES('".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."');");

//log stuff searched for
dbQuery("INSERT INTO searchlogs_verses(book,chapter,verse) VALUES('$book','$chapter','$verse');");


if($verse>0){
		//complete jewish bible
		$res1 =dbQuery("SELECT versenum,verse FROM cjb
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
		//good news translation
		$res2 =dbQuery("SELECT versenum,verse FROM gnt
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
		//king james version
		$res3 =dbQuery("SELECT versenum,verse FROM kjv
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
		//the message
		$res4 =dbQuery("SELECT versenum,verse FROM message
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
		//new american standard bible
		$res5 =dbQuery("SELECT versenum,verse FROM nasb
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");

		//new international readers version
		$res6 =dbQuery("SELECT versenum,verse FROM nirv
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
		//neew international version
		$res7 =dbQuery("SELECT versenum,verse FROM niv
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
		//new international version (uk)
		$res8 =dbQuery("SELECT versenum,verse FROM nivuk
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
								
		//new living translation
		$res9 =dbQuery("SELECT versenum,verse FROM nlt
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");

		//new revised standard version
		$res10 =dbQuery("SELECT versenum,verse FROM nrsv
								WHERE versenum=$verse
								AND chapternum=$chapter
								AND book='$book';");
	
}else{
	//complete jewish bible
		$res1 =dbQuery("SELECT versenum,verse FROM cjb
								WHERE chapternum=$chapter
								AND book='$book';");
		//good news translation
		$res2 =dbQuery("SELECT versenum,verse FROM gnt
								WHERE chapternum=$chapter
								AND book='$book';");
		//king james version
		$res3 =dbQuery("SELECT versenum,verse FROM kjv
								WHERE chapternum=$chapter
								AND book='$book';");
		//the message
		$res4 =dbQuery("SELECT versenum,verse FROM message
								WHERE chapternum=$chapter
								AND book='$book';");
		//new american standard bible
		$res5 =dbQuery("SELECT versenum,verse FROM nasb
								WHERE chapternum=$chapter
								AND book='$book';");

		//new international readers version
		$res6 =dbQuery("SELECT versenum,verse FROM nirv
								WHERE chapternum=$chapter
								AND book='$book';");
		//neew international version
		$res7 =dbQuery("SELECT versenum,verse FROM niv
								WHERE chapternum=$chapter
								AND book='$book';");
		//new international version (uk)
		$res8 =dbQuery("SELECT versenum,verse FROM nivuk
								WHERE chapternum=$chapter
								AND book='$book';");
								
		//new living translation
		$res9 =dbQuery("SELECT versenum,verse FROM nlt
								WHERE chapternum=$chapter
								AND book='$book';");

		//new revised standard version
		$res10 =dbQuery("SELECT versenum,verse FROM nrsv
								WHERE chapternum=$chapter
								AND book='$book';");
}



?>  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!-- SKILLS -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			Search Result For <?php if ($verse>0){ echo $book.' '.$chapter.':'.$verse;}else{echo $book.' '.$chapter;} ?>
	  			</h2>
	  			<p class="center">The results of your search are listed below.</p>
	  			<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li><a href="#version1" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> Complete Jewish Bible</a></li>
						<li class="active"><a href="#version2" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> Good News</a></li>
						<li><a href="#version3" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> KJV</a></li>
						<li><a href="#version4" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> The Message</a></li>
						<li><a href="#version5" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> NAST</a></li>
						<li><a href="#version6" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> NIRV</a></li>
						<li><a href="#version7" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> NIV</a></li>
						<li><a href="#version8" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> NIV (UK)</a></li>
						<li><a href="#version9" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> NLT</a></li>
						<li><a href="#version10" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i> NRSV</a></li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane bounceInRight" id="version1">
							<h2><i class="fa fa-envelope-o"></i>Complete Jewish Bible</h2>
							<br>
							<br>
				  			<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res1)>0){
										if($verse>0){
											while($row=dbFetchAssoc($res1)){ ?>
											<p><?php 
													extract($row);
													echo $verse; ?></p>
											
											<?php
												
											}
										}
										else{
											while($row=dbFetchAssoc($res1)){ ?>
											<p><?php 
													extract($row);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
										}
										
									} ?>
				  			</p>
							
							<div class="text-left">
								<?php if(dbNumRows($res2)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php }} ?>
							</div>
						</div>
						<!--good news-->
						<div class="tab-pane active bounceInRight" id="version2">
							<h2><i class="fa fa-envelope-o"></i>Good News Translation</h2>
							<br>
							<br>
				  			<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res2)>0){
										if($verse>0){
											while($row2=dbFetchAssoc($res2)){ ?>
											<p><?php 
													extract($row2);
													echo $verse; ?></p>
											
											<?php
												
											}
										}
										else{
											while($row2=dbFetchAssoc($res2)){ ?>
											<p><?php 
													extract($row2);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
										}
									}									?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res3)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php } }?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version3">
							<h2><i class="fa fa-envelope-o"></i>King James Version</h2>
							<br>
							<br>
				  			<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res3)>0){
										if($verse>0){
											while($row3=dbFetchAssoc($res3)){ ?>
											<p><?php 
													extract($row3);
													echo $verse; ?></p>
											
											<?php
												
											}
										}else{
											while($row3=dbFetchAssoc($res3)){ ?>
											<p><?php 
													extract($row3);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
											
										}
										
									} ?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res4)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php }} ?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version4">
							<h2><i class="fa fa-envelope-o"></i>The Message</h2>
							<br>
							<br>
							<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res4)>0){
										if($verse>0){
											while($row4=dbFetchAssoc($res4)){ ?>
											<p><?php 
													extract($row4);
													echo $verse; ?></p>
											
											<?php
												
											}
										}
										else{
											while($row4=dbFetchAssoc($res4)){ ?>
											<p><?php 
													extract($row4);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
											
										}
										
									} ?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res5)>0){if($versenum>0){ ?>
									<a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a>
								<?php } }?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version5">
							<h2><i class="fa fa-envelope-o"></i>New American Standard Translation</h2>
							<br>
							<br>
							<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res5)>0){
										if($verse>0){
											while($row5=dbFetchAssoc($res5)){ ?>
											<p><?php 
													extract($row5);
													echo $verse; ?></p>
											
											<?php
												
											}
										}else{
											while($row5=dbFetchAssoc($res5)){ ?>
											<p><?php 
													extract($row5);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
										}
										
									} ?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res6)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php }} ?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version6">
							<h2><i class="fa fa-envelope-o"></i>New International Reader's Version</h2>
							<br>
							<br>
							<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res6)>0){
										if($verse>0){
											while($row6=dbFetchAssoc($res6)){ ?>
											<p><?php 
													extract($row6);
													echo $verse; ?></p>
											
											<?php
												
											}
										}else{
											while($row6=dbFetchAssoc($res6)){ ?>
											<p><?php 
													extract($row6);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
										}
										
									} ?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res7)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php }} ?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version7">
							<h2><i class="fa fa-envelope-o"></i>New International Version</h2>
							<br>
							<br>
							<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res7)>0){
										if($verse>0){
											while($row7=dbFetchAssoc($res7)){ ?>
											<p><?php 
													extract($row7);
													echo $verse; ?></p>
											
											<?php
												
											}
										}
										else{
											while($row7=dbFetchAssoc($res7)){ ?>
											<p><?php 
													extract($row7);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
										}
										
									} ?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res8)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php }} ?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version8">
							<h2><i class="fa fa-envelope-o"></i>New International Version (UK)</h2>
							<br>
							<br>
						<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res8)>0){
										if($verse>0){
											while($row8=dbFetchAssoc($res8)){ ?>
											<p><?php 
													extract($row8);
													echo $verse; ?></p>
											
											<?php
												
											}
										}else{
											while($row8=dbFetchAssoc($res8)){ ?>
											<p><?php 
													extract($row8);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
										}
										
									} ?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res9)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php }} ?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version9">
							<h2><i class="fa fa-envelope-o"></i>New Living Translation</h2>
							<br>
							<br>
							<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res9)>0){
										if($verse>0){
											while($row9=dbFetchAssoc($res9)){ ?>
											<p><?php 
													extract($row9);
													echo $verse; ?></p>
											
											<?php
												
											}
										}
										else{
											while($row9=dbFetchAssoc($res9)){ ?>
											<p><?php 
													extract($row9);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
											
										}
										
									} ?>
				  			</p>
							<div class="text-left">
								<?php if(dbNumRows($res10)>0){if($versenum>0){ ?><a href="?passage=<?php echo $book; ?>&chapter=<?php echo $chapter; ?>" class="btn btn-default"><i class="fa fa-home"></i> View Passage</i></a><?php }} ?>
							</div>
						</div>
						<div class="tab-pane bounceInRight" id="version10">
							<h2><i class="fa fa-envelope-o"></i>New Revised Standard Version</h2>
							<br>
							<br>
							<p class="text-justify"> 
							<?php 											
									if(dbNumRows($res10)>0){
										if($verse>0){
											while($row10=dbFetchAssoc($res10)){ ?>
											<p><?php 
													extract($row10);
													echo $verse; ?></p>
											
											<?php
												
											}
										}else{
											while($row10=dbFetchAssoc($res10)){ ?>
											<p><?php 
													extract($row10);
													echo '<a href="?book='.$book.'&chapter='.$chapter.'&verse='.$versenum.'">'.$versenum.'</a> '.$verse; ?></p>
											
											<?php
												
											}
										}
										
									} ?>
				  			</p>
							
						</div>
					</div>
					<script>
					  $(function () {
					    	$('#SkillsTab a').click(function (e) {
						  		e.preventDefault()
						  		$(this).tab('show')
							});
					  });
					</script>
	  			</div>
	  		</div>
	  		<div class="container">
	  			<!-- CUSTOMER REVIEWS -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			Get In Touch<span>What are you waiting for ?</span>
	  			</h2>
	  		</div>
	  		<!-- CUSTOM CONTAINER -->
	  		<section class="contact-container with-separation-bottom with-separation-top">
		  		<div class="contact-boxes">
		  			<div class="contact-box contact-box-email animate-me zoomIn">
		  				<h2>Email</h2>
		  				<p>Write us a few lines about your ideas, your projects and let's advance together.</p>
		  				<a href="#" class="btn btn-default"><i class="fa fa-envelope"></i> Email Us</a>
		  			</div>
		  			<div class="contact-box contact-box-twitter animate-me zoomIn">
		  				<h2>Twitter</h2>
		  				<p>Follow us to interact, chat and share our ideas on Twitter.</p>
		  				<a href="#" class="btn btn-default"><i class="fa fa-twitter"></i> Follow us</a>
		  			</div>
		  			<div class="contact-box contact-box-facebook animate-me zoomIn">
		  				<h2>Facebook</h2>
		  				<p>Like our page and send us message directly from our brand new Facebook page.</p>
		  				<a href="#" class="btn btn-default"><i class="fa fa-facebook"></i> Like Our Page</a>
		  			</div>
		  			<div class="contact-box contact-box-skype animate-me zoomIn">
		  				<h2>Skype</h2>
		  				<p>Call us or chat with us on Skype whenever you like, do not hesitate.</p>
		  				<a href="#" class="btn btn-default"><i class="fa fa-skype"></i> Call Us</a>
		  			</div>
		  		</div>
	  		</section>
	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	
	 