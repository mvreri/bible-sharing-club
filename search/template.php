<?php

if(session_id() == '')
    session_start();

// For debugging. You can get rid of these two lines safely
//    error_reporting(E_ALL);
//    ini_set('display_errors', 1);

file_exists(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'handler.php') ? require_once(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'handler.php') : die('There is no such a file: handler.php');
file_exists(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'config.php') ? require_once(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'config.php') : die('There is no such a file: config.php');



?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <!-- PAGE TITLE -->
    <title>Bible Sharing Club | Bible Verses, Passages, Inspiration</title>
    <!-- MAKE IT RESPONSIVE -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- SEO -->
    <meta name="description" content="A place to share bible verses, share the bible, tweet verses, a verse a day">
    <meta name="author" content="@zer0chills_">
    <meta name="keywords" content="tweet verse, share verse, bible, verse, memory verse, verse of the day, bible inspiration, share inspiration, meditate, word of God">
    <!-- FAVICON -->	
	<link rel="shortcut icon" href="images/favicon.png" />
    <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="style.css" rel="stylesheet" media="screen">
    <link href="css/options.css" rel="stylesheet" media="screen">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
	
	<!-- Live Search Styles -->
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
	
    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:900,300,400,200,800' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

	<script src="js/jquery-1.11.1.min.js"></script><script type="text/javascript" src="js/selectshare.js"></script>
    <script>var twitterAccount = "bs_c_";</script>
    <div class="shareTooltip" id="shareTooltip">
         <div class="tooltipContainer"><a id="sendToTwitter" href="" class="sharingLink twitter"><span></span></a></div>
     </div>
     <!-- Twitter code to open a new dialog window -->
     <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    </head>
  <!-- START BODY -->
  <body>
  	<!-- LOADER DIV - ONLY HOME -->
  	<div id="loader">
  		<div id="loading-logo"></div>
  	</div>
  	<div id="page">
	  	<!-- START HEADER -->
	  	<!-- START HEADER -->
	  	<header id="header" class="small with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  		<!-- TOP NAVIGATION -->
	  		<div id="top-navigation">
		  		<ul class="animate-me fadeInDown" data-wow-duration="1.2s">
			  		<!--<li class="menu-item"><i class="fa fa-phone"></i> 00 11 22 33 44</li>
			  		<li class="menu-item"><a href="faq.html"><i class="fa fa-question"></i> FAQ</a></li>
			  		<li class="menu-item"><a href="page.html"><i class="fa fa-link"></i> Just a link</a></li>
			  		<li class="menu-item"><span class="navigation-social">We're social !</span> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-youtube"></i></a></li>
			  		<li class="menu-item">
			  			<!-- SEARCHFORM -->
			  			<!--<div id="search-container" class="animate-me fadeInDown">
					  		<form role="search" method="get" action="#" >
								<input type="text" value="" name="s" id="s" placeholder="Search..."/>
								<input type="hidden" name="searchsubmit" id="searchsubmit" value="true" />
								<button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>
					        </form>
				  		</div>
			  			<a href="#" id="search-toggle"><i class="fa"></i></a>
			  		</li>
			  		<li class="menu-item menu-item-has-children language-switcher">
			  			<!-- LANGUAGES -->
			  			<!--<a href="#"><i class="fa fa-flag"></i> English <i class="fa fa-angle-down"></i></a>
				  		<ul class="sub-menu bounceInDown">
				  			<li class="menu-item"><a href="#">French</a></li>
				  			<li class="menu-item"><a href="#">Chinese</a></li>
				  			<li class="menu-item"><a href="#">German</a></li>
			  			</ul>
			  		</li>-->
		  		</ul>
	  		</div>
	  	
	  		<!-- MOBILE NAVIGATION -->
	  		<nav id="navigation-mobile"></nav>
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			<a href="#" id="show-mobile-menu"><i class="fa fa-bars"></i></a>
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<ul id="left-navigation" class="animate-me fadeInLeftBig">
			  		
		  		</ul>
		  		<div  class="animate-me flipInX" data-wow-duration="5s">
		  			<a href="../" id="logo-navigation">
		  				<span>Find It. Share It.</span>
		  			</a>
		  		</div>
		  		<ul id="right-navigation" class="animate-me fadeInRightBig">
			  		
		  		</ul>
	  		</nav>

	  		<!-- SHADOW -->
	  		<div id="shade"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/backgrounds/bg1.jpg" alt="SLider Image"></li>
		  		</ul>	
		  	</div>
	  		
	  	</header>
	  	
	  	<!-- END HEADER -->
		
		
		
		<br><br>
		<br><br>
		
		<!-- Search Form Demo -->
	<div class="ls_container">

    <!-- Search Form -->
    <form accept-charset="UTF-8" class="search" id="ls_form" name="ls_form">
    	<?php 
    		// Set javascript anti bot value in the session
    		Handler::get_javascript_anti_bot(); 
    	?>
        <input type="hidden" name="ls_anti_bot" id="ls_anti_bot" value="">
        <input type="hidden" name="ls_token" id="ls_token" value="<?php echo Handler::get_token(); ?>">
        <input type="hidden" name="ls_page_loaded_at" id="ls_page_loaded_at" value="<?php echo time(); ?>">
        <input type="hidden" name="ls_current_page" id="ls_current_page" value="1">
        <input type="text" name="ls_query" id="ls_query" placeholder="Search bible text" autocomplete="off" maxlength="<?php echo Config::MAX_INPUT_LENGTH; ?>">

        <!-- Result -->
        <div id="ls_result_div">
            <div id="ls_result_main">
                <table>
                    <tbody>

                    </tbody>
                </table>
            </div>

            <!-- Pagination -->
            <div id="ls_result_footer">
                <div class="col page_limit">
                    <select id="ls_items_per_page" name="ls_items_per_page">
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="0">All</option>
                    </select>
                </div>
                <div class="col navigation">
                    <i class="icon-left-circle arrow" id="ls_previous_page"></i>
                </div>
                <div class="col navigation pagination">
                    <label id="ls_current_page_lbl">1</label> / <label id="ls_last_page_lbl"></label>
                </div>
                <div class="col navigation">
                    <i class="icon-right-circle arrow" id="ls_next_page"></i>
                </div>

            </div>

        </div>

    </form>

</div>
<!-- /Search Form Demo -->
		
	  	
	  	<?php require_once $content; ?>
	  	
	  	<!-- START FOOTER -->
	  	<footer id="footer" class="with-separation-top">
		  	<aside id="widgets" class="container">
		  		<div class="row">
		  			<!-- WIDGET -->
			  		<div class="col-md-4 widget animate-me fadeInLeft">
			  			<img src="images/logo-footer.png" id="footer-logo" alt="Logo footer">
			  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ullamcorper felis pulvinar neque pharetra auctor. Donec vel erat pretium, interdum justo sed, mollis tortor.</p>
			  			<a href="about.html" class="btn btn-default"><i class="fa fa-users"></i> Read more</a>
			  		</div>
		  			<!-- WIDGET -->
			  		<div class="col-md-4 widget animate-me fadeInUp">
				  		<h4>Navigation</h4>
				  		<ul>
					  		<li><a href="#">Home</a></li>
					  		<li><a href="portfolio.html">Portfolio</a></li>
					  		<li><a href="blog.html">Blog</a></li>
					  		<li><a href="about.html">About Us</a></li>
				  		</ul>
			  		</div>
		  			<!-- WIDGET -->
			  		<div class="col-md-4 widget animate-me fadeInRight">
			  			<h4>Contact</h4>
			  			<ul class="contact-informations">
				  			<li class="contact-address">666 Avenue des Champs-�lys�es. 75000 Paris</li>
				  			<li class="contact-phone">(+33)0 11 22 33 44</li>
			  			</ul>
			  			<ul class="widget-social">
			  				<!-- ALL ICONS AVAILABLE ->http://fortawesome.github.io/Font-Awesome/icons/#brand-->
				  			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				  			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				  			<li><a href="#"><i class="fa fa-skype"></i></a></li>
				  			<li><a href="#"><i class="fa fa-rss"></i></a></li>
			  			</ul>
			  			<a href="contact.html" class="btn btn-default"><i class="fa fa-envelope-o"></i> Contact Form</a>
			  		</div>
		  		</div>
		  	</aside>
		  	<div id="copyright" class="animate-me fadeInUp">
		  		<div class="container">
			  		<p>&#169; 2015 All Rights Reserved. Powered by <a href="http://cronzen.co.ke">CRONZEN</a>.</p>
			  		
		  		</div>
		  	</div>
	  	</footer>
	  	<!-- END FOOTER -->
	  	
	  	<!-- SCROLL TOP -->
	  	<a href="#" id="scroll-top" class="fadeInRight animate-me"><i class="fa fa-angle-double-up"></i></a>
  	</div>


<!-- Live Search Script -->
<script type="text/javascript" src="js/script.min.js"></script>

    <!-- SCRIPTS -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/plugins.js"></script>
	<script type="text/javascript">
		/*TEXT TICKER (ONLY FOR HOME PAGE)*/
		$('#ticker-text').vTicker('init', {
			speed: 300, 
		    pause: 2000
	    });
	</script>
    <script src="js/custom.js"></script>
  </body>
  <!-- END BODY -->
</html>