<?php

if(count(get_included_files()) === 1)
    exit("Direct access not permitted.");

class Config
{
    // ***** Database ***** //
    const HOST = 'localhost';
    const DATABASE = 'biblos';
    const USERNAME = 'root';
    const PASS = '';

    // ***** Table ***** //
    const USER_TABLE = 'kjv';
    const SEARCH_COLUMN = 'verse';

    // ***** Form ***** //
    // This must be the same as form_anti_bot in script.min.js or script.js
    const ANTI_BOT = "Ehsan's guard";

    // Assigning more than 3 seconds is not recommended
    const SEARCH_START_TIME_OFFSET = 3;

    // ***** Search Input ***** //
    const MAX_INPUT_LENGTH = 40;
}