<?php
require_once 'include/configf.php';
if(session_id() == '')
    session_start();

// For debugging. You can get rid of these two lines safely
//    error_reporting(E_ALL);
//    ini_set('display_errors', 1);

file_exists(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'handler.php') ? require_once(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'handler.php') : die('There is no such a file: handler.php');
file_exists(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'config.php') ? require_once(__DIR__.DIRECTORY_SEPARATOR.'class'.DIRECTORY_SEPARATOR.'config.php') : die('There is no such a file: config.php');

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <!-- PAGE TITLE -->
    <title>Bible Sharing Club | Bible Verses, Passages, Inspiration</title>
    <!-- MAKE IT RESPONSIVE -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- SEO -->
    <meta name="description" content="A place to share bible verses, share the bible, tweet verses, a verse a day">
    <meta name="author" content="@zer0chills_">
    <meta name="keywords" content="tweet verse, share verse, bible, verse, memory verse, verse of the day, bible inspiration, share inspiration, meditate, word of God">
    <!-- FAVICON -->	
	<link rel="shortcut icon" href="images/favicon.png" />
    <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="style.css" rel="stylesheet" media="screen">
    <link href="css/options.css" rel="stylesheet" media="screen">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
	
	<!-- Live Search Styles -->
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="css/style.min.css">
	
    <!-- FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:900,300,400,200,800' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

	<script src="js/jquery-1.11.1.min.js"></script><script type="text/javascript" src="js/selectshare.js"></script>
    <script>var twitterAccount = "bs_c_";</script>
    <div class="shareTooltip" id="shareTooltip">
         <div class="tooltipContainer"><a id="sendToTwitter" href="" class="sharingLink twitter"><span></span></a></div>
     </div>
     <!-- Twitter code to open a new dialog window -->
     <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    </head>
  <!-- START BODY -->
  <body>
  	<!-- LOADER DIV - ONLY HOME -->
  	<div id="loader">
  		<div id="loading-logo"></div>
  	</div>
  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="big with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  	
	  		<!-- MOBILE NAVIGATION -->
	  		<nav id="navigation-mobile"></nav>
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			<a href="#" id="show-mobile-menu"><i class="fa fa-bars"></i></a>
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<!--<ul id="left-navigation" class="animate-me fadeInLeftBig">
			  		<li class="menu-item current-menu-item menu-item-has-children">
			  			<a href="#" data-description="All starts here">Home</a>
			  			<ul class="sub-menu bounceInDown">
			  				<li class="menu-item"><a href="#"></a></li>
				  			<li class="menu-item"><a href="#"></a></li>
				  			<li class="menu-item"><a href="#"></a></li>
				  			<li class="menu-item"><a href="#"><i class="fa fa-star"></i> </a></li>
			  			</ul>
			  		</li>
			  		<li class="menu-item menu-item-has-children">
			  			<a href="#" data-description="What we do">Portfolio</a>
			  			<ul class="sub-menu bounceInDown">
			  				<li class="menu-item"><a href="#"></a></li>
				  			<li class="menu-item"><a href="#"></a></li>
				  			<li class="menu-item"><a href="#l"></a></li>
			  			</ul>
			  		</li>
			  		<li class="menu-item"><a href="" data-description="We're the best"></a></li>
		  		</ul>-->
                <ul id="left-navigation" class="animate-me fadeInLeftBig">
			  		
		  		</ul>
		  		<div class="animate-me flipInX" data-wow-duration="5s">
		  			<a href="#" id="logo-navigation">
		  				<span>Find It. Share It.</span>
		  			</a>
		  		</div>
                <ul id="right-navigation" class="animate-me fadeInRightBig">
			  				  		</ul>
		  		<!--<ul id="right-navigation" class="animate-me fadeInRightBig">
			  		<li class="menu-item"><a href="#" data-description="We are a team">About</a></li>
			  		<li class="menu-item menu-item-has-children">
			  			<a href="#" data-description="We share">Blog</a>
			  			<ul class="sub-menu bounceInDown">
				  			<li class="menu-item"><a href="#">All Posts</a></li>
				  			<li class="menu-item"><a href="#">Single Post</a></li>
			  			</ul>
			  		</li>
			  		<li class="menu-item"><a href="#" data-description="Let's get in touch">Contact</a></li>
		  		</ul>-->
	  		</nav>
	  		
	  		<!-- TEXT SLIDER -->
			<div id="ticker" class="animate-me zoomIn">
				<h4 class="with-breaker">The scripture says...</h4>
				<div id="ticker-text">
					<ul>
						<li><a href=""><?php $sq0 = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <50 AND verse like '%love%' ORDER BY rand() LIMIT 1;");
											while($row0=mysqli_fetch_assoc($sq0))
											{ extract($row0);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></a></li>
						<li><?php $sq1 = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <70 AND verse like '%pray%' ORDER BY rand() LIMIT 1;");
											while($row1=mysqli_fetch_assoc($sq1))
											{ extract($row1);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></li>
						<li><?php $sq = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <50 AND verse like '%bless%' ORDER BY rand() LIMIT 1;");
											while($row=mysqli_fetch_assoc($sq))
											{ extract($row);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></li>
						<li><?php $sqb = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <50 AND verse like '%afraid%' ORDER BY rand() LIMIT 1;");
											while($rowb=mysqli_fetch_assoc($sqb))
											{ extract($rowb);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></li>
						<li><?php $sqc = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <50 AND verse like '%miracle%' ORDER BY rand() LIMIT 1;");
											while($rowc=mysqli_fetch_assoc($sqc))
											{ extract($rowc);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></li>
						<li><?php $sqd = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <80 AND book like '%psalms%'  ORDER BY rand() LIMIT 1;");
											while($rowd=mysqli_fetch_assoc($sqd))
											{ extract($rowd);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></li>
						<li><?php $sqe = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <50 AND verse like '%faith%' ORDER BY rand() LIMIT 1;");
											while($rowe=mysqli_fetch_assoc($sqe))
											{ extract($rowe);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></li>
                         <li><?php $sqf = mysqli_query($conn,"SELECT verseid, verse, book, chapternum,versenum FROM gnt WHERE length(verse) <70 AND book like '%proverbs%'  ORDER BY rand() LIMIT 1;");
											while($rowf=mysqli_fetch_assoc($sqf))
											{ extract($rowf);
											echo $verse.' ~ '.$book.' '.$chapternum.':'.$versenum;} ?></li>
					</ul>
				</div>
			</div>
  		
	  		
	  		<!-- SCROLL BOTTOM -->
	  		<div id="scroll-bottom" class="animate-me fadeInUpBig">
		  		<a href="#"><i class="fa fa-angle-double-down"></i></a>
	  		</div>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade"></div>

	  		<!-- SLIDER NAVIGATION (DELETE IF YOU DON'T USE THE SLIDER) -->
            <div class="navigation-slider-container">
            	<a href="#" id="sliderPrev"><i class="fa fa-arrow-left"></i></a>
            	<a href="#" id="sliderNext"><i class="fa fa-arrow-right"></i></a>
            </div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/backgrounds/bg1.jpg" alt="SLider Image"></li>
		  			<li><img src="images/backgrounds/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/backgrounds/bg3.jpg" alt="SLider Image"></li>
		  		</ul>	
		  	</div>
		  	<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
		  	<!--<div id="header-video"
			    data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>-->
	  		
	  	</header>
	  	<!-- END HEADER -->
		
		
		
		<br><br>
		<br><br>
		
		<!-- Search Form Demo -->
	<div class="ls_container">

    <!-- Search Form -->
    <form accept-charset="UTF-8" class="search" id="ls_form" name="ls_form">
    	<?php 
    		// Set javascript anti bot value in the session
    		Handler::get_javascript_anti_bot(); 
    	?>
        <input type="hidden" name="ls_anti_bot" id="ls_anti_bot" value="">
        <input type="hidden" name="ls_token" id="ls_token" value="<?php echo Handler::get_token(); ?>">
        <input type="hidden" name="ls_page_loaded_at" id="ls_page_loaded_at" value="<?php echo time(); ?>">
        <input type="hidden" name="ls_current_page" id="ls_current_page" value="1">
        <input type="text" name="ls_query" id="ls_query" placeholder="Search bible text" autocomplete="off" maxlength="<?php echo Config::MAX_INPUT_LENGTH; ?>">

        <!-- Result -->
        <div id="ls_result_div">
            <div id="ls_result_main">
                <table>
                    <tbody>

                    </tbody>
                </table>
            </div>

            <!-- Pagination -->
            <div id="ls_result_footer">
                <div class="col page_limit">
                    <select id="ls_items_per_page" name="ls_items_per_page">
                        <option value="5" selected>5</option>
                        <option value="10">10</option>
                        <option value="0">All</option>
                    </select>
                </div>
                <div class="col navigation">
                    <i class="icon-left-circle arrow" id="ls_previous_page"></i>
                </div>
                <div class="col navigation pagination">
                    <label id="ls_current_page_lbl">1</label> / <label id="ls_last_page_lbl"></label>
                </div>
                <div class="col navigation">
                    <i class="icon-right-circle arrow" id="ls_next_page"></i>
                </div>

            </div>

        </div>

    </form>

</div>
<!-- /Search Form Demo -->
		
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!-- SKILLS -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			What do we do <span>Find Your Solace</span>
	  			</h2>
	  			<table class="skills">
		  			<tr id="skills-container" class="skills-row">
			  			<td class="skill animate-me zoomIn">
				  			<h4><i class="fa fa-star-o"></i>Encouragement</h4>
				  			<p>A pick me up, straight from the Book of Life</p>
			  			</td>
			  			<td class="skill animate-me zoomIn">
				  			<h4><i class="fa fa-envelope-o"></i>Empowerment</h4>
				  			<p>Get a kickstart to your day with a word from the bible</p>
			  			</td>
			  			<td class="skill animate-me zoomIn">
				  			<h4><i class="fa fa-line-chart"></i>Enhanced, In-depth finds</h4>
				  			<p>Find bible scriptures on different bible versions, and share the finds</p>
			  			</td>
		  			</tr>
		  			
	  			</table>
	  			<!--<div class="skill-button center animate-me fadeInUp">
	  				<a href="skills.html" class="btn btn-default"><i class="fa fa-trophy"></i> Check Out our skills</a>
	  			</div>-->
	  		</div>
	  		<!-- CUSTOM CONTAINER -->
	  		<section class="custom-section-container with-separation-bottom with-separation-top">
		  		<div class="container">
			  		<div class="custom-section-text">
				  		<h2 class="animate-me fadeInLeft">Become an Affiliate</h2>
				  		<p class="animate-me fadeInLeft">Tweet Us at @b_sc_</p>
				  		
			  		</div>
			  		<div class="custom-section-buttons">
				  		<a href="#" class="btn btn-default animate-me fadeInRight"><i class="fa fa-book"></i> Learn More</a>
				  		<a href="#" class="btn btn-default animate-me fadeInRight"><i class="fa fa-paper-plane"></i> Ask your question</a>
			  		</div>
		  		</div>
	  		</section>

	  		
	  		<!-- CUSTOM CONTAINER -->
	  		<section class="contact-container with-separation-bottom with-separation-top">
		  		<div class="contact-boxes">
		  			<div class="contact-box contact-box-email animate-me zoomIn">
		  				<h2>Email</h2>
		  				<p>Write us a few lines about your ideas, your projects and let's advance together.</p>
		  				<a href="contact.html" class="btn btn-default"><i class="fa fa-envelope"></i> Email Us</a>
		  			</div>
		  			<div class="contact-box contact-box-twitter animate-me zoomIn">
		  				<h2>Twitter</h2>
		  				<p>Follow us to interact, chat and share our ideas on Twitter.</p>
		  				<a href="#" class="btn btn-default"><i class="fa fa-twitter"></i> Follow us</a>
		  			</div>
		  			<div class="contact-box contact-box-facebook animate-me zoomIn">
		  				<h2>Facebook</h2>
		  				<p>Like our page and send us message directly from our brand new Facebook page.</p>
		  				<a href="#" class="btn btn-default"><i class="fa fa-facebook"></i> Like Our Page</a>
		  			</div>
		  			<div class="contact-box contact-box-skype animate-me zoomIn">
		  				<h2>Skype</h2>
		  				<p>Call us or chat with us on Skype whenever you like, do not hesitate.</p>
		  				<!-- GET THE LINL -> http://www.skype.com/en/features/skype-buttons/create-skype-buttons/--> 
		  				<a href="#" class="btn btn-default"><i class="fa fa-skype"></i> Call Us</a>
		  			</div>
		  		</div>
	  		</section>
	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	
	  	<!-- START FOOTER -->
	  	<footer id="footer" class="with-separation-top">
		  	<aside id="widgets" class="container">
		  		<div class="row">
		  			<!-- WIDGET -->
			  		<div class="col-md-4 widget animate-me fadeInLeft">
			  			<img src="images/logo-footer.png" id="footer-logo" alt="Logo footer">
			  			<p>We believe in the word of God. Everyone should be able to read and understand all scripture.</p>
			  			<!--<a href="" class="btn btn-default"><i class="fa fa-users"></i> Read more</a>-->
			  		</div>
		  			<!-- WIDGET -->
			  		<div class="col-md-4 widget animate-me fadeInUp">
				  		<h4>Navigation</h4>
				  		<ul>
					  		<li><a href="#">Home</a></li>
					  		<li><a href="/search">Find</a></li>
					  		<li><a href="#">Blog (Coming Soon)</a></li>
					  		
				  		</ul>
			  		</div>
		  			<!-- WIDGET -->
			  		<div class="col-md-4 widget animate-me fadeInRight">
			  			<h4>Contact</h4>
			  			<ul class="contact-informations">
				  			<li class="contact-address">Nairobi, Kenya</li>
				  			<li class="contact-phone">(+254) 734 085 087</li>
			  			</ul>
			  			<ul class="widget-social">
			  				<!-- ALL ICONS AVAILABLE ->http://fortawesome.github.io/Font-Awesome/icons/#brand-->
				  			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				  			<li><a href="twitter.com/b_sc_"><i class="fa fa-twitter"></i></a></li>
				  			<li><a href="#"><i class="fa fa-skype"></i></a></li>
				  			<li><a href="#"><i class="fa fa-rss"></i></a></li>
			  			</ul>
			  			<!--<a href="contact.html" class="btn btn-default"><i class="fa fa-envelope-o"></i> Contact Form</a>-->
			  		</div>
		  		</div>
		  	</aside>
		  	<div id="copyright" class="animate-me fadeInUp">
		  		<div class="container">
			  		<p>&#169; 2015 All Rights Reserved. Powered by <a href="http://cronzen.co.ke">CRONZEN</a>.</p>
			  		
		  		</div>
		  	</div>
	  	</footer>
	  	<!-- END FOOTER -->
	  	
	  	<!-- SCROLL TOP -->
	  	<a href="#" id="scroll-top" class="fadeInRight animate-me"><i class="fa fa-angle-double-up"></i></a>
  	</div>


<!-- Live Search Script -->
<script type="text/javascript" src="js/script.min.js"></script>

    <!-- SCRIPTS -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/plugins.js"></script>
	<script type="text/javascript">
		/*TEXT TICKER (ONLY FOR HOME PAGE)*/
		$('#ticker-text').vTicker('init', {
			speed: 300, 
		    pause: 8000
	    });
	</script>
    <script src="js/custom.js"></script>
  </body>
  <!-- END BODY -->
</html>